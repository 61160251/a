const profileController ={
    profileList:[
        // {name:"james",gender: 'M'}
    ],
    addProfile(profile){
        this.profileList.push(profile)
        return profile
    },
    updateProfile(profile){
        this.profileList.splice(0,1,profile)
        return profile
    },
    deleteProfile(profile){
        this.profileList.splice(0,1)
        return profile
    },
    getProfile(){
        return [...this.profileList]
    }
}

module.exports = profileController