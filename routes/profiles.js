const express = require('express')
const router = express.Router()
const profilesController = require('../controller/ProfileController')


router.get('/',(req, res, next) => {
  res.json(profilesController.getProfile())
})

router.post('/',(req, res, next) => {
  const payload = req.body
  res.json(profilesController.addProfile(payload))
})

router.put('/',(req,res,next)=>{
  const payload = req.body
  res.json(profilesController.updateProfile(payload))
})

router.delete('/',(req,res,next)=>{
  const payload = req.body
  res.json(profilesController.deleteProfile())
})

module.exports = router
